import React from 'react';
import './index.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';

function Modal(props) {
  setTimeout(() => {
    const fullScreen = document.querySelector('.v360-fullscreen-toggle');
    fullScreen.remove();
  }, 50);
  const backdropStyle = {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    background: 'rgba(49,49,49,0.6)',
    padding: '100px',
  };

  // The modal "window"
  const modalStyle = {
    position: 'fixed',
    width: '90%',
    height: '90vh',
    top: '50%',
    left: '50%',
    background: '#f2f0f0',
    transform: 'translate(-50%,-50%)',
    borderRadius: '6px',
    overflow: 'hidden',
    objectPosition: 'center top',
  };

  const footerStyle = {
    display: 'grid',
  };

  return (
    <div style={backdropStyle}>
      <div style={modalStyle}>
        <div style={footerStyle}>
          <div onClick={props.closeModal} className='close-button'>
            <FontAwesomeIcon icon={faXmark} />
          </div>
        </div>
        {props.children}
      </div>
    </div>
  );
}

class ModalOLD extends React.Component {
  render() {
    // setTimeout(() => {
    //   const fullScreen = document.querySelector('.v360-fullscreen-toggle');
    //   fullScreen.remove();
    // }, 50);

    // The gray background
    const backdropStyle = {
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      background: 'rgba(49,49,49,0.6)',
      padding: '100px',
    };

    // The modal "window"
    const modalStyle = {
      position: 'fixed',
      width: '90%',
      height: '90vh',
      top: '50%',
      left: '50%',
      background: '#f2f0f0',
      transform: 'translate(-50%,-50%)',
      borderRadius: '6px',
      overflow: 'hidden',
      objectPosition: 'center top',
    };

    const footerStyle = {
      display: 'grid',
    };

    return (
      <div style={backdropStyle}>
        <div style={modalStyle}>
          <div style={footerStyle}>
            <div onClick={this.props.onClose} className='close-button'>
              <FontAwesomeIcon icon={faXmark} />
            </div>
          </div>
          {this.props.children}
        </div>
      </div>
    );
  }
}

export default Modal;
