import React from 'react';
import ThreeSixty from 'react-360-view';

class ImageViewer extends React.Component {
  render() {
    return (
      <ThreeSixty
        amount={this.props.viewer.amount}
        imagePath={this.props.viewer.imagePath}
        fileName={this.props.viewer.fileName}
      />
    );
  }
}

export default ImageViewer;
