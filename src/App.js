import React, { useState } from 'react';
import image from './bg.png';
import buttonimage from './360.png';
import Modal from './modal';
import Viewer from './threeSixty';
import './index.css';

const items = [
  {
    amount: 36,
    imagePath:
      'https://scaleflex.cloudimg.io/crop/1920x900/n/https://scaleflex.airstore.io/demo/360-car',
    fileName: 'iris-{index}.jpeg',
  },
  {
    amount: 38,
    imagePath: '/images',
    fileName: '{index}.jpeg',
  },
  {
    amount: 30,
    imagePath: '/images',
    fileName: '{index}.jpeg',
  },
  {
    amount: 19,
    imagePath: '/images',
    fileName: '{index}.jpeg',
  },
];

const Button = (props) => {
  return (
    <div onClick={props.openModal} className='open-button'>
      <div
        style={{
          backgroundImage: `url(${buttonimage})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'contain',
          backgroundPosition: 'center center',
          height: '100%',
          width: '100%',
        }}
      />
      360°
    </div>
  );
};

function App() {
  const [open, setOpen] = useState(false);
  const [item, setItem] = useState({});

  function openModal(item) {
    setOpen(true);
    setItem(item);
  }

  function closeModal() {
    setOpen(false);
  }

  const textStyle = {
    color: '#4a4949',
    position: 'absolute',
    bottom: '36.5%',
    left: '22.5%',
    fontSize: '14px',
  };

  return (
    <div
      style={{
        backgroundImage: `url(${image})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'contain',
        height: '100vh',
        width: '100%',
      }}
    >
      <span style={textStyle}>360°-Viewer:</span>

      <div className='wrapper-grid'>
        {items.map((item, key) => (
          <Button key={key} openModal={() => openModal(item)} />
        ))}
      </div>
      {open && (
        <Modal closeModal={() => closeModal()}>
          <Viewer viewer={item} />
        </Modal>
      )}
    </div>
  );
}

export default App;
